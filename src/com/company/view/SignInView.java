package com.company.view;

import com.company.GameApplication;
import com.company.service.SignInService;
import com.company.service.SignInServiceImpl;
import com.company.DTO.PlayerDTO;
import com.company.helpers.PasswordHelper;
import com.company.model.PlayerConfig;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class SignInView extends BaseView {

    private final String title = "Sign in";
    private AnchorPane pane = null;
    private TextField nicknameField;
    private TextField passwordField;
    private Text signInExceptionArea;
    private Button signInButton;
    private Hyperlink signUpLink;
    private VBox vBox;
    private final GameApplication application = getApplication();
    private final SignInService<PlayerDTO> service = new SignInServiceImpl();

    public String getTitle() {
        return title;
    }

    private final EventHandler<ActionEvent> signInEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (signInButton == actionEvent.getSource()) {

                PlayerDTO player = service.get(nicknameField.getText());

                if (player == null) {
                    signInExceptionArea.setText("No player with this nickname.");
                    application.setApplicationHeight(230);

                } else if (!player.getPassword().equals(PasswordHelper.encrypt(passwordField.getText()))) {
                    signInExceptionArea.setText("Wrong password.");

                    application.setApplicationHeight(230);

                } else {
                    application.setUserConfig(new PlayerConfig(nicknameField.getText()));
                    application.setView(application.getMenuView());
                    nicknameField.setText("");
                    passwordField.setText("");

                }

            }
        }
    };

    private final EventHandler<ActionEvent> signUpEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (signUpLink == actionEvent.getSource()) {
                application.setView(application.getSignUpView());
            }
        }
    };

    public SignInView() throws Exception {
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    private void createView() {
        pane = new AnchorPane();

        vBox = new VBox(5);

        signUpLink = new Hyperlink("sign up");
        signUpLink.setOnAction(signUpEvent);

        Label nicknameLabel = new Label("Nickname");
        nicknameField = new TextField();

        signInButton = new Button("start");
        signInButton.setOnAction(signInEvent);


        Label passwordLabel = new Label("Password");
        passwordField = new TextField();

        signInExceptionArea = new Text();

        AnchorPane.setTopAnchor(vBox, 5.0);
        AnchorPane.setLeftAnchor(vBox, 10.0);
        AnchorPane.setRightAnchor(vBox, 10.0);

        vBox.getChildren().addAll(nicknameLabel, nicknameField, passwordLabel,
                passwordField, signInButton, signUpLink, signInExceptionArea);

        pane.getChildren().add(vBox);

        application.setApplicationSize(235, 210);

    }

}



























