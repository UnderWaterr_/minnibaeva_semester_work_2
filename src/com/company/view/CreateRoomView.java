package com.company.view;

import com.company.GameApplication;
import com.company.multiplayer.client.ChatClient;
import com.company.multiplayer.server.ChatServer;
import com.company.multiplayer.server.GameStarter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class CreateRoomView extends BaseView {

    public String title = "Create room";
    private int lvlNumber = 1;

    private AnchorPane pane = null;

    private VBox createRoomBox;

    private Button createRoomButton;
    private final GameApplication application = getApplication();
    private Button menuButton;


    private final EventHandler<ActionEvent> createRoomEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (createRoomButton == actionEvent.getSource()) {

                int port = (int) Math.floor(Math.random() * (5000 - 4000 + 1) + 4000);

                ChatServer chatServer = new ChatServer(port);
                application.setChatServer(chatServer);
                new Thread(new GameStarter(chatServer)).start();

                application.chatClient = new ChatClient(application);

                try {
                    application.startChatClient(port);
                    application.setView(new WaitOpponentView(port, lvlNumber));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final EventHandler<ActionEvent> menuButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (menuButton == actionEvent.getSource()) {
                try {
                    application.setView(new MenuView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    public CreateRoomView() throws Exception {
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void createView() {
        ObservableList<String> levelList = FXCollections.observableArrayList("1", "2", "3");
        ComboBox<String> levelComboBox = new ComboBox<>(levelList);
        levelComboBox.setValue("1");

        levelComboBox.setOnAction(event -> lvlNumber = Integer.parseInt(levelComboBox.getValue()));

        Label chooseLvlLabel = new Label("Choose level number");


        createRoomButton = new Button("create room");
        createRoomButton.setOnAction(createRoomEvent);

        pane = new AnchorPane();

        menuButton = new Button("menu");
        menuButton.setOnAction(menuButtonEvent);

        createRoomBox = new VBox(5);
        createRoomBox.getChildren().addAll(menuButton, chooseLvlLabel, levelComboBox, createRoomButton);
        AnchorPane.setLeftAnchor(createRoomBox, 30.0);
        AnchorPane.setTopAnchor(createRoomBox, 30.0);

        pane.getChildren().add(createRoomBox);


        application.getStage().setWidth(250);
        application.getStage().setHeight(250);

    }

}
