package com.company.view;

import com.company.GameApplication;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class MenuView extends BaseView {

    private final String title = "Menu";
    private AnchorPane pane = null;
    private VBox vBox;
    private Text nickname;
    private Button singlePlayer;
    private Button multiplayer;
    private Button skin;
    private Button exit;

    private final GameApplication application = getApplication();

    private final EventHandler<ActionEvent> singlePlayerEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (singlePlayer == actionEvent.getSource()) {
                try {
                    application.setView(new LevelListView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final EventHandler<ActionEvent> multiplayerEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (multiplayer == actionEvent.getSource()) {
                try {
                    application.setView(new MultiplayerLobbyView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };


    private final EventHandler<ActionEvent> skinEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (skin == actionEvent.getSource()) {
                try {
                    application.setView(new SkinView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final EventHandler<ActionEvent> exitEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (exit == actionEvent.getSource()) {
                application.getStage().close();
            }
        }
    };


    public MenuView() throws Exception {
    }

    public String getTitle() {
        return title;
    }

    public Text getNickname() {
        return nickname;
    }


    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    private void createView() {
        pane = new AnchorPane();

        vBox = new VBox(5);

        nickname = new Text(application.getUserConfig().getNickname());

        Font font = Font.font("Courier New", FontWeight.BOLD, 20);

        singlePlayer = new Button("singlePlayer");
        singlePlayer.setOnAction(singlePlayerEvent);

        singlePlayer.setMaxWidth(1000);
        singlePlayer.setMaxHeight(2000);
        singlePlayer.setFont(font);

        multiplayer = new Button("multiplayer");
        multiplayer.setOnAction(multiplayerEvent);

        multiplayer.setMaxWidth(1000);
        multiplayer.setMaxHeight(2000);
        multiplayer.setFont(font);

        skin = new Button("skin");
        skin.setOnAction(skinEvent);

        skin.setMaxWidth(1000);
        skin.setMaxHeight(2000);
        skin.setFont(font);

        exit = new Button("exit");
        exit.setOnAction(exitEvent);

        exit.setMaxWidth(1000);
        exit.setMaxHeight(2000);
        exit.setFont(font);

        vBox.getChildren().addAll(nickname, singlePlayer, multiplayer, skin, exit);

        AnchorPane.setTopAnchor(vBox, 5.0);
        AnchorPane.setLeftAnchor(vBox, 10.0);
        AnchorPane.setRightAnchor(vBox, 10.0);

        pane.getChildren().add(vBox);

        application.getStage().setWidth(220);
        application.getStage().setHeight(240);
    }


}
