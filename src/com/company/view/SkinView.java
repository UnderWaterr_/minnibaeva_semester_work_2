package com.company.view;

import com.company.GameApplication;
import com.company.service.PassedLevelsService;
import com.company.service.PassedLevelsServiceImpl;
import com.company.service.SkinService;
import com.company.service.SkinServiceImpl;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SkinView extends BaseView {

    private final String title = "Skin";
    private AnchorPane pane = null;

    private Button menuButton;
    private Button buySkinButton;
    private Button setFoxSkinButton;
    private Button setGreyFoxSkinButton;
    HBox hBox;
    Text skinChoose;
    Text noCoins;
    VBox vBox1;
    VBox vBox2;
    ImageView fox;
    ImageView greyFox;
    private Button coinInfo;

    private final GameApplication application = getApplication();

    private final SkinService service = new SkinServiceImpl();
    public final PassedLevelsService serviceCoin = new PassedLevelsServiceImpl();

    private final EventHandler<ActionEvent> menuButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (menuButton == actionEvent.getSource()) {
                try {
                    application.setView(new MenuView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    private final EventHandler<ActionEvent> buySkinEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (buySkinButton == actionEvent.getSource()) {
                noCoins.setText("");
                application.getStage().setHeight(150);
                if (serviceCoin.getCoins(application.getPlayerConfig().getNickname()) < 4) {
                    noCoins.setText("You don't have 4 coins");
                    application.getStage().setWidth(310);
                    application.getStage().setHeight(180);

                    return;
                }

                serviceCoin.saveCoins(application.getPlayerConfig().getNickname(), -4);
                coinInfo.setText(Integer.toString(serviceCoin.getCoins(application.getPlayerConfig().getNickname())));
                service.buySkin(application.getPlayerConfig().getNickname());
                vBox2.getChildren().remove(buySkinButton);
                vBox2.getChildren().remove(noCoins);
                vBox2.getChildren().add(setGreyFoxSkinButton);
            }
        }

    };

    private final EventHandler<ActionEvent> setFoxSkinEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (setFoxSkinButton == actionEvent.getSource()) {
                noCoins.setText("");
                application.getStage().setWidth(250);
                application.getStage().setHeight(150);

                service.setSkin(application.getPlayerConfig().getNickname(), 1);

                vBox2.getChildren().remove(skinChoose);
                vBox1.getChildren().add(skinChoose);

                vBox1.getChildren().remove(setFoxSkinButton);
                vBox2.getChildren().add(setGreyFoxSkinButton);

            }
        }

    };

    private final EventHandler<ActionEvent> setGreyFoxSkinEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (setGreyFoxSkinButton == actionEvent.getSource()) {
                noCoins.setText("");
                application.getStage().setWidth(250);
                application.getStage().setHeight(150);

                service.setSkin(application.getPlayerConfig().getNickname(), 2);

                vBox2.getChildren().add(skinChoose);
                vBox1.getChildren().remove(skinChoose);

                vBox1.getChildren().add(setFoxSkinButton);
                vBox2.getChildren().remove(setGreyFoxSkinButton);
                application.getStage().setWidth(290);
                application.getStage().setHeight(150);

            }
        }

    };


    public SkinView() throws Exception {
    }


    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }


    @Override
    public String getTitle() {
        return title;
    }


    private void createView() {
        menuButton = new Button("menu");
        menuButton.setOnAction(menuButtonEvent);

        vBox1 = new VBox(5);
        vBox1.setMinWidth(150);
        vBox1.setMaxWidth(150);

        vBox2 = new VBox(5);
        vBox2.setMinWidth(150);
        vBox2.setMaxWidth(150);

        pane = new AnchorPane();
        hBox = new HBox(5);

        noCoins = new Text("");

        buySkinButton = new Button("buy skin");
        buySkinButton.setOnAction(buySkinEvent);

        setFoxSkinButton = new Button("set skin");
        setFoxSkinButton.setOnAction(setFoxSkinEvent);

        setGreyFoxSkinButton = new Button("set skin");
        setGreyFoxSkinButton.setOnAction(setGreyFoxSkinEvent);

        try {
            fox = new ImageView(new Image(new FileInputStream("src/resources/22.png")));
            greyFox = new ImageView(new Image(new FileInputStream("src/resources/23.png")));

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        fox.setFitHeight(40);
        fox.setFitWidth(56);
        greyFox.setFitHeight(40);
        greyFox.setFitWidth(57);
        skinChoose = new Text("You choose this skin");

        vBox1.getChildren().add(fox);
        vBox2.getChildren().add(greyFox);


        if (service.getSkin(application.getPlayerConfig().getNickname()) == 1) {
            vBox1.getChildren().add(skinChoose);
            application.getStage().setWidth(250);
            application.getStage().setHeight(150);

            if (service.isSkinBuying(application.getPlayerConfig().getNickname())) {
                vBox2.getChildren().add(setGreyFoxSkinButton);
            } else {
                vBox2.getChildren().addAll(buySkinButton, noCoins);
            }
        } else {
            vBox2.getChildren().add(skinChoose);
            vBox1.getChildren().add(setFoxSkinButton);
            application.getStage().setWidth(290);
            application.getStage().setHeight(150);

        }

        coinInfo = new Button(serviceCoin.getCoins(application.getPlayerConfig().getNickname()) + " coin");
        hBox.getChildren().addAll(vBox1, vBox2);
        pane.getChildren().addAll(menuButton, hBox, coinInfo);

        AnchorPane.setTopAnchor(hBox, 40.0);
        AnchorPane.setLeftAnchor(hBox, 10.0);
        AnchorPane.setRightAnchor(hBox, 10.0);

        AnchorPane.setLeftAnchor(menuButton, 170.0);
        AnchorPane.setTopAnchor(menuButton, 10.0);

        AnchorPane.setLeftAnchor(coinInfo, 10.0);
        AnchorPane.setTopAnchor(coinInfo, 10.0);

    }
}
