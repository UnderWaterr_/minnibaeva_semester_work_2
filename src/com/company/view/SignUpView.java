package com.company.view;

import com.company.GameApplication;
import com.company.DTO.PlayerDTO;
import com.company.service.SignUpService;
import com.company.service.SignUpServiceImpl;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class SignUpView extends BaseView {

    private final String title = "Sign up";
    private AnchorPane pane = null;
    private Text signUpExceptionArea;
    private TextField nicknameField;
    private TextField passwordField;
    private Button signUpButton;
    private Hyperlink signInLink;

    private VBox vBox;
    private final GameApplication application = getApplication();
    private final SignUpService<PlayerDTO> service = new SignUpServiceImpl();

    public String getTitle() {
        return title;
    }

    private final EventHandler<ActionEvent> signUpEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (signUpButton == actionEvent.getSource()) {
                signUpExceptionArea.setText("");

                if (nicknameField.getText().length() < 3) {
                    signUpExceptionArea.setText("Nickname must be longer than 2 characters.");
                    application.setApplicationSize(270, 230);

                }
                if (passwordField.getText().length() < 5) {
                    if (signUpExceptionArea.getText().length() > 0) {
                        signUpExceptionArea.setText(signUpExceptionArea.getText()
                                + "\nPassword must be longer than 4 characters.");
                        application.setApplicationSize(270, 250);

                    } else {
                        signUpExceptionArea.setText("Password must be longer than 4 characters.");
                        application.setApplicationSize(270, 230);

                    }
                }

                if (signUpExceptionArea.getText().length() == 0) {
                    PlayerDTO player = new PlayerDTO(nicknameField.getText(), passwordField.getText());

                    boolean isSaved = service.save(player);
                    if (!isSaved) {
                        signUpExceptionArea.setText("This player nickname is already exists");
                        application.setApplicationSize(270, 230);
                    } else {
                        application.setApplicationSize(235, 210);
                        nicknameField.setText("");
                        passwordField.setText("");
                        application.setView(application.getSignInView());
                    }
                }
            }
        }
    };

    private final EventHandler<ActionEvent> signInEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (signInLink == actionEvent.getSource()) {
                application.setView(application.getSignInView());
            }
        }
    };

    public SignUpView() throws Exception {
    }


    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    private void createView() {
        pane = new AnchorPane();

        vBox = new VBox(5);


        Label nicknameLabel = new Label("Nickname");
        nicknameField = new TextField();

        Label passwordLabel = new Label("Password");
        passwordField = new TextField();

        signUpButton = new Button("sign up");
        signUpButton.setOnAction(signUpEvent);

        signUpExceptionArea = new Text();

        signInLink = new Hyperlink("sign in");
        signInLink.setOnAction(signInEvent);

        vBox.getChildren().addAll(nicknameLabel, nicknameField, passwordLabel,
                passwordField, signUpButton, signInLink, signUpExceptionArea);


        AnchorPane.setTopAnchor(vBox, 5.0);
        AnchorPane.setLeftAnchor(vBox, 10.0);
        AnchorPane.setRightAnchor(vBox, 10.0);

        pane.getChildren().add(vBox);

        application.setApplicationSize(235, 210);
    }

}
