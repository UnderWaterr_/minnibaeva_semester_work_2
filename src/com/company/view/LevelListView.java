package com.company.view;

import com.company.GameApplication;
import com.company.DTO.PlayerPassedLevelsDTO;
import com.company.service.PassedLevelsService;
import com.company.service.PassedLevelsServiceImpl;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class LevelListView extends BaseView {
    private final String title = "Level list";

    private AnchorPane pane = null;
    private HBox hBox;
    private VBox vBox1;
    private VBox vBox2;
    private VBox vBox3;

    private Text level1Text;
    private Text level2Text;
    private Text level3Text;

    private Button level1Button;
    private Button level2Button;
    private Button level3Button;
    private Button menuButton;

    private final PassedLevelsService service = new PassedLevelsServiceImpl();
    private final GameApplication application = getApplication();

    private final EventHandler<ActionEvent> menuButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (menuButton == actionEvent.getSource()) {
                try {
                    application.setView(new MenuView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    private final EventHandler<ActionEvent> level1ButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (level1Button == actionEvent.getSource()) {
                application.startGame(1);
            }
        }
    };

    private final EventHandler<ActionEvent> level2ButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (level2Button == actionEvent.getSource()) {
                application.startGame(2);
            }
        }
    };

    private final EventHandler<ActionEvent> level3ButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (level3Button == actionEvent.getSource()) {
                application.startGame(3);
            }
        }
    };

    public LevelListView() throws Exception {
    }


    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }


    @Override
    public String getTitle() {
        return title;
    }


    private void createView() {
        pane = new AnchorPane();

        hBox = new HBox(5);
        vBox1 = new VBox(5);
        vBox1.setMinWidth(150);
        vBox1.setMaxWidth(150);

        vBox2 = new VBox(5);
        vBox2.setMinWidth(150);
        vBox2.setMaxWidth(150);

        vBox3 = new VBox(5);
        vBox3.setMinWidth(150);
        vBox3.setMaxWidth(150);

        level1Button = new Button("Start level");
        level1Button.setOnAction(level1ButtonEvent);

        level2Button = new Button("Start level");
        level2Button.setOnAction(level2ButtonEvent);

        level3Button = new Button("Start level");
        level3Button.setOnAction(level3ButtonEvent);


        level1Text = new Text(1 + " " + lvlTime(1));

        level2Text = new Text(2 + " " + lvlTime(2));

        level3Text = new Text(3 + " " + lvlTime(3));

        menuButton = new Button("menu");
        menuButton.setOnAction(menuButtonEvent);


        vBox1.getChildren().addAll(level1Text, level1Button);
        vBox2.getChildren().addAll(level2Text, level2Button);
        vBox3.getChildren().addAll(level3Text, level3Button);

        hBox.getChildren().addAll(vBox1, vBox2, vBox3);

        pane.getChildren().addAll(menuButton, hBox);

        AnchorPane.setTopAnchor(hBox, 40.0);
        AnchorPane.setLeftAnchor(hBox, 10.0);
        AnchorPane.setRightAnchor(hBox, 10.0);

        AnchorPane.setLeftAnchor(menuButton, 380.0);
        AnchorPane.setTopAnchor(menuButton, 10.0);


        application.getStage().setWidth(450);
        application.getStage().setHeight(130);

    }

    private String lvlTime(int lvlNumber) {
        PlayerPassedLevelsDTO passedLevelsDao = service.get(application.getMenuView().getNickname().getText(), lvlNumber);

        if (passedLevelsDao == null) {
            return "Level is not passed";
        } else {
            return ((double) passedLevelsDao.getLevel().getResultTime() / 1000) + " sec";
        }
    }
}
