package com.company.view;

import com.company.GameApplication;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

public class WaitOpponentView extends BaseView {

    public String title = "Wait opponent";
    private final int lvlNumber;

    private AnchorPane pane = null;

    private VBox createRoomBox;

    private Text wait;

    private final GameApplication application = getApplication();

    private final int port;
    private Button menuButton;

    private final EventHandler<ActionEvent> menuButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (menuButton == actionEvent.getSource()) {
                try {
                    application.setView(new MenuView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    public WaitOpponentView(int port, int lvlNumber) throws Exception {
        this.port = port;
        this.lvlNumber = lvlNumber;
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }


    @Override
    public String getTitle() {
        return title;
    }

    private void createView() {
        pane = new AnchorPane();

        createRoomBox = new VBox(5);

        menuButton = new Button("menu");
        menuButton.setOnAction(menuButtonEvent);


        wait = new Text("Waiting for opponent \nRoom number " + port);

        createRoomBox.getChildren().addAll(menuButton, wait);
        AnchorPane.setLeftAnchor(createRoomBox, 50.0);
        AnchorPane.setTopAnchor(createRoomBox, 50.0);

        pane.getChildren().add(createRoomBox);


        application.getStage().setWidth(250);
        application.getStage().setHeight(250);


        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                if (application.chatServer.getClients().size() == 2) {
                    application.getChatClient()
                            .sendMessage("1 " + lvlNumber + " " + application.getPlayerConfig().getNickname() + "\n");
                }

                if (application.secondPlayerName != null) {
                    application.startMultiplayerGame(lvlNumber);
                    this.stop();
                }

            }
        };

        timer.start();

    }
}
