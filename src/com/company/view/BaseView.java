package com.company.view;

import com.company.GameApplication;
import javafx.scene.Parent;

public abstract class BaseView {

    private static GameApplication application;

    public abstract Parent getView();

    public static void setApplication(GameApplication application) {
        BaseView.application = application;
    }

    public static GameApplication getApplication() throws Exception {
        if (application != null) {
            return application;
        }
        throw new Exception("No Application in BaseView");
    }

    public abstract String getTitle();
}
