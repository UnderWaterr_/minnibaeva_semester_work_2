package com.company.view;

import com.company.GameApplication;
import com.company.multiplayer.client.ChatClient;
import javafx.animation.AnimationTimer;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;

public class MultiplayerLobbyView extends BaseView {
    public String title = "Lobby";

    private AnchorPane pane = null;
    private Button createRoomButton;
    private Button connectButton;
    private Button menuButton;
    private VBox createRoomBox;
    private TextArea portTextArea;

    private final GameApplication application = getApplication();

    private final EventHandler<ActionEvent> createRoomEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (createRoomButton == actionEvent.getSource()) {
                try {
                    application.setView(new CreateRoomView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private final EventHandler<ActionEvent> connectButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (connectButton == actionEvent.getSource()) {

                application.chatClient = new ChatClient(application);

                try {
                    application.startChatClient(Integer.parseInt(portTextArea.getText()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                AnimationTimer timer = new AnimationTimer() {
                    @Override
                    public void handle(long now) {

                        if (application.lvlNumber != 0) {
                            application.getChatClient()
                                    .sendMessage("1 " + application.lvlNumber + " " + application.getPlayerConfig().getNickname() + " " + "\n");

                            application.startMultiplayerGame(application.lvlNumber);
                            this.stop();
                        }
                    }
                };

                timer.start();

            }

        }
    };

    private final EventHandler<ActionEvent> menuButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (menuButton == actionEvent.getSource()) {
                try {
                    application.setView(new MenuView());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    };

    public MultiplayerLobbyView() throws Exception {
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView();
        }

        return pane;
    }

    @Override
    public String getTitle() {
        return title;
    }

    private void createView() {
        pane = new AnchorPane();

        createRoomBox = new VBox(5);
        createRoomBox.setMinWidth(150);
        createRoomBox.setMaxWidth(150);
        AnchorPane.setLeftAnchor(createRoomBox, 15.0);
        AnchorPane.setTopAnchor(createRoomBox, 15.0);

        createRoomButton = new Button("Create room");
        createRoomButton.setOnAction(createRoomEvent);

        connectButton = new Button("connect");
        connectButton.setOnAction(connectButtonEvent);

        menuButton = new Button("menu");
        menuButton.setOnAction(menuButtonEvent);
        portTextArea = new TextArea();
        portTextArea.setPromptText("Room number");

        portTextArea.setMaxHeight(25);
        portTextArea.setMinHeight(25);
        createRoomBox.getChildren().addAll(createRoomButton, portTextArea, connectButton, menuButton);
        pane.getChildren().add(createRoomBox);


        application.getStage().setWidth(220);
        application.getStage().setHeight(220);
    }
}
