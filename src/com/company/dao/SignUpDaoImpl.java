package com.company.dao;

import com.company.helpers.PostgresConnectionHelper;
import com.company.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SignUpDaoImpl implements SignUpDao<Player> {
    public static final Logger LOGGER = LoggerFactory.getLogger(SignUpDaoImpl.class);

    private final Connection connection = PostgresConnectionHelper.getConnection();

    //language=SQL
    private final String SQL_SAVE_CLIENT = "insert into player (nickname, password) values (?, ?)";

    @Override
    public boolean save(Player player) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_CLIENT);
            preparedStatement.setString(1, player.getNickname());
            preparedStatement.setString(2, player.getPassword());
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Failed to save new player.", e);
            return false;
        }

    }
}
