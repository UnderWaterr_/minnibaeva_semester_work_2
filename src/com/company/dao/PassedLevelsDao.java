package com.company.dao;

public interface PassedLevelsDao<T> {
    boolean save(T t);

    T get(String nickname, int levelNumber);

    boolean saveCoins(String nickname, int coin);

    int getSkin(String nickname);

    int getCoins(String nickname);
}
