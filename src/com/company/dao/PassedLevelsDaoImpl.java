package com.company.dao;

import com.company.DTO.LevelDTO;
import com.company.DTO.PlayerPassedLevelsDTO;
import com.company.helpers.PostgresConnectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PassedLevelsDaoImpl implements PassedLevelsDao<PlayerPassedLevelsDTO> {

    public static final Logger LOGGER = LoggerFactory.getLogger(PassedLevelsDaoImpl.class);

    private final Connection connection = PostgresConnectionHelper.getConnection();

    //language=SQL
    private final String SQL_SAVE_LEVEL_INFORMATION = "insert into passed_level (nickname, level_number, time) VALUES (?, ?, ?)";

    //language=SQL
    private final String SQL_GET_LEVEL_INFO = "select * from passed_level where nickname = ? and level_number = ?";


    //language=SQL
    private final String SQL_UPDATE_LEVEL_INFORMATION = "update passed_level set time = ? where nickname = ? and level_number = ?";

    //language=SQL
    private final String SQL_UPDATE_COINS = "update player set coin = (select coin from player where nickname = ?) + ? where nickname = ?";

  //language=SQL
    private final String SQL_GET_SKIN = "select * from player where nickname = ?";

  //language=SQL
    private final String SQL_GET_COIN = "select * from player where nickname = ?";


    @Override
    public boolean save(PlayerPassedLevelsDTO playerPassedLevelsDTO) {
        try {
            if (get(playerPassedLevelsDTO.getNickname(), playerPassedLevelsDTO.getLevel().getLevelNumber()) != null) {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_LEVEL_INFORMATION);
                preparedStatement.setLong(1, playerPassedLevelsDTO.getLevel().getResultTime());
                preparedStatement.setString(2, playerPassedLevelsDTO.getNickname());
                preparedStatement.setInt(3, playerPassedLevelsDTO.getLevel().getLevelNumber());
                preparedStatement.executeUpdate();
                return true;
            } else {
                PreparedStatement preparedStatement = connection.prepareStatement(SQL_SAVE_LEVEL_INFORMATION);
                preparedStatement.setString(1, playerPassedLevelsDTO.getNickname());
                preparedStatement.setInt(2, playerPassedLevelsDTO.getLevel().getLevelNumber());
                preparedStatement.setLong(3, playerPassedLevelsDTO.getLevel().getResultTime());
                preparedStatement.executeUpdate();
                return true;
            }

        } catch (SQLException e) {
            LOGGER.warn("Failed to save passed level.", e);
            return false;
        }
    }

    @Override
    public PlayerPassedLevelsDTO get(String nickname, int levelNumber) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_LEVEL_INFO);
            preparedStatement.setString(1, nickname);
            preparedStatement.setInt(2, levelNumber);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new PlayerPassedLevelsDTO(
                        resultSet.getString("nickname"),
                        new LevelDTO(
                                resultSet.getInt("level_number"),
                                resultSet.getLong("time")
                        )
                );
            }
            return null;
        } catch (SQLException e) {
            LOGGER.warn("Failed to get level.", e);
        }
        return null;
    }

    @Override
    public boolean saveCoins(String nickname, int coin) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_UPDATE_COINS);
            preparedStatement.setString(1, nickname);

            preparedStatement.setInt(2, coin);
            preparedStatement.setString(3, nickname);
            preparedStatement.executeUpdate();
            return true;


        } catch (SQLException e) {
            LOGGER.warn("Failed to save coins.", e);
            return false;
        }
    }

    @Override
    public int getSkin(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_SKIN);
            preparedStatement.setString(1, nickname);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return  resultSet.getInt("skin_number");
            }
            return 0;
        } catch (SQLException e) {
            LOGGER.warn("Failed to get skin.", e);
        }
        return 0;
    }

    @Override
    public int getCoins(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_COIN);
            preparedStatement.setString(1, nickname);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return  resultSet.getInt("coin");
            }
            return 0;
        } catch (SQLException e) {
            LOGGER.warn("Failed to get coin.", e);
        }
        return -1;
    }
}
