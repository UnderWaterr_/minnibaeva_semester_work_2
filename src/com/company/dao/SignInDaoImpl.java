package com.company.dao;

import com.company.helpers.PostgresConnectionHelper;
import com.company.model.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SignInDaoImpl implements SignInDao<Player> {

    public static final Logger LOGGER = LoggerFactory.getLogger(SignInDaoImpl.class);

    private final Connection connection = PostgresConnectionHelper.getConnection();

    //language=SQL
    private final String SQL_SELECT_FOR_EMAIL = "select * from player where nickname = ?";

    @Override
    public Player getPlayer(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_FOR_EMAIL);
            preparedStatement.setString(1, nickname);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return new Player(
                        resultSet.getString("nickname"),
                        resultSet.getString("password")
                );
            }
            return null;
        } catch (SQLException e) {
            LOGGER.warn("Failed to get player.", e);
        }
        return null;
    }

}
