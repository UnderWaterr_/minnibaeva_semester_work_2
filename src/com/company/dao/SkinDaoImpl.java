package com.company.dao;

import com.company.helpers.PostgresConnectionHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SkinDaoImpl implements SkinDao {

    public static final Logger LOGGER = LoggerFactory.getLogger(SkinDaoImpl.class);

    private final Connection connection = PostgresConnectionHelper.getConnection();

    //language=SQL
    private final String SQL_BUY_SKIN = "update player set is_skin_buyed = true where nickname = ? ";

    //language=SQL
    private final String SQL_IS_SKIN_BUYING = "select * from player where nickname = ?";


    //language=SQL
    private final String SQL_SET_SKIN = "update player set skin_number = ? where nickname = ?";

    //language=SQL
    private final String SQL_GET_SKIN = "select * from player where nickname = ?";


    @Override
    public boolean buySkin(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_BUY_SKIN);
            preparedStatement.setString(1, nickname);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Failed to save skin.", e);
            return false;
        }

    }

    @Override
    public boolean isSkinBuying(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_IS_SKIN_BUYING);
            preparedStatement.setString(1, nickname);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getBoolean("is_skin_buyed");
            }
            return false;
        } catch (SQLException e) {
            LOGGER.warn("Failed to buy skin.", e);
        }
        return false;
    }

    @Override
    public boolean setSkin(String nickname, int skinNumber) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SET_SKIN);
            preparedStatement.setInt(1, skinNumber);
            preparedStatement.setString(2, nickname);
            preparedStatement.executeUpdate();
            return true;
        } catch (SQLException e) {
            LOGGER.warn("Failed to set skin.", e);
            return false;
        }

    }

    @Override
    public int getSkin(String nickname) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_GET_SKIN);
            preparedStatement.setString(1, nickname);
            ResultSet resultSet = preparedStatement.executeQuery();

            if (resultSet.next()) {
                return resultSet.getInt("skin_number");
            }
            return 0;
        } catch (SQLException e) {
            LOGGER.warn("Failed to get skin.", e);
            return 0;
        }

    }
}
