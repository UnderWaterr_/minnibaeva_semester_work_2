package com.company.dao;

public interface SkinDao {
     boolean buySkin(String nickname);
    boolean isSkinBuying(String nickname);
    boolean setSkin(String nickname, int skinNumber);
    int getSkin(String nickname);
}
