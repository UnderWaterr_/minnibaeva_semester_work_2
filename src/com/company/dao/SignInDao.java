package com.company.dao;

public interface SignInDao<T> {
    T getPlayer(String nickname);
}
