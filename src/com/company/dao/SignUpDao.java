package com.company.dao;

public interface SignUpDao<T> {
    boolean save(T t);
}
