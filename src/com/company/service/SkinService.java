package com.company.service;

public interface SkinService {
    void buySkin(String nickname);
    boolean isSkinBuying(String nickname);
    void setSkin(String nickname, int skinNumber);
    int getSkin(String nickname);
}
