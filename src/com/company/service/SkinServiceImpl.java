package com.company.service;

import com.company.dao.SkinDao;
import com.company.dao.SkinDaoImpl;

public class SkinServiceImpl implements SkinService {
    private final SkinDao dao = new SkinDaoImpl();

    @Override
    public void buySkin(String nickname) {
        dao.buySkin(nickname);
    }

    @Override
    public boolean isSkinBuying(String nickname) {
        return dao.isSkinBuying(nickname);
    }

    @Override
    public void setSkin(String nickname, int skinNumber) {
        dao.setSkin(nickname, skinNumber);
    }

    @Override
    public int getSkin(String nickname) {
        return dao.getSkin(nickname);
    }
}
