package com.company.service;

import com.company.DTO.PlayerDTO;
import com.company.dao.SignUpDao;
import com.company.dao.SignUpDaoImpl;
import com.company.helpers.PasswordHelper;
import com.company.model.Player;

public class SignUpServiceImpl implements SignUpService<PlayerDTO> {

    private final SignUpDao<Player> dao = new SignUpDaoImpl();

    @Override
    public boolean save(PlayerDTO playerDTO) {
        Player player = new Player(playerDTO.getNickname(), PasswordHelper.encrypt(playerDTO.getPassword()));
        return dao.save(player);
    }

}
