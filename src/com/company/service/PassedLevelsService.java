package com.company.service;

import com.company.DTO.PlayerPassedLevelsDTO;

public interface PassedLevelsService {
    boolean save(String nickname, int levelNumber, long time);

    PlayerPassedLevelsDTO get(String nickname, int levelNumber);

    void saveCoins(String nickname, int coin);
    int getCoins(String nickname);

    int getSkin(String nickname);
}
