package com.company.service;


import com.company.DTO.PlayerDTO;
import com.company.dao.SignInDao;
import com.company.dao.SignInDaoImpl;
import com.company.model.Player;

public class SignInServiceImpl implements SignInService<PlayerDTO> {

    private final SignInDao<Player> dao = new SignInDaoImpl();

    @Override
    public PlayerDTO get(String nickname) {
        Player player = dao.getPlayer(nickname);

        if (player != null) {
            return new PlayerDTO(player.getNickname(),
                    player.getPassword()
            );
        }
        return null;
    }

}
