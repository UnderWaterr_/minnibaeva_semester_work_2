package com.company.service;

public interface SignInService<T> {
    T get(String email);
}
