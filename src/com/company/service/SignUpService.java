package com.company.service;

public interface SignUpService<T> {
    boolean save(T t);
}
