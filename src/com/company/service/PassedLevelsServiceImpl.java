package com.company.service;

import com.company.DTO.LevelDTO;
import com.company.DTO.PlayerPassedLevelsDTO;
import com.company.dao.PassedLevelsDao;
import com.company.dao.PassedLevelsDaoImpl;

public class PassedLevelsServiceImpl implements PassedLevelsService {

    private final PassedLevelsDao<PlayerPassedLevelsDTO> dao = new PassedLevelsDaoImpl();

    @Override
    public boolean save(String nickname, int levelNumber, long time) {
        PlayerPassedLevelsDTO playerPassedLevelsDTO = new PlayerPassedLevelsDTO(nickname,
                new LevelDTO(levelNumber, time));

        return dao.save(playerPassedLevelsDTO);
    }

    @Override
    public PlayerPassedLevelsDTO get(String nickname, int levelNumber) {
        return dao.get(nickname, levelNumber);
    }

    @Override
    public void saveCoins(String nickname, int coin) {
        dao.saveCoins(nickname, coin);
    }

    @Override
    public int getCoins(String nickname) {
        return dao.getCoins(nickname);
    }

    @Override
    public int getSkin(String nickname) {
        return dao.getSkin(nickname);
    }
}
