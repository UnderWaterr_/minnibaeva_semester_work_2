package com.company.multiplayer;

import com.company.GameApplication;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import static com.company.view.BaseView.getApplication;

public class GameMenuMultiplayerPane extends Pane {
    private final GameApplication application = getApplication();
    private final MultiplayerView gameView;
    private BorderPane menu;

    Button exit;
    Button continueLvl;
    VBox vBox;

    Font font = Font.font("Courier New", FontWeight.BOLD, 30);

    private final EventHandler<ActionEvent> exitButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (exit == actionEvent.getSource()) {
                application.getMultiplayerView().timer.stop();
                application.endMultiplayerGame();
             }
        }
    };

    private final EventHandler<ActionEvent> continueLvlButtonEvent = new EventHandler<>() {
        @Override
        public void handle(ActionEvent actionEvent) {
            if (continueLvl == actionEvent.getSource()) {
                gameView.hideMenu();
             }
        }
    };


    public GameMenuMultiplayerPane(MultiplayerView gameView) throws Exception {
        this.gameView = gameView;
    }


    public BorderPane createMenu() {


        continueLvl = new Button("continue");
        continueLvl.setFont(font);
        continueLvl.setPrefSize(250, 50);
        continueLvl.setOnAction(continueLvlButtonEvent);

        exit = new Button("exit");

        exit.setFont(font);
        exit.setPrefSize(250, 50);
        exit.setOnAction(exitButtonEvent);

        vBox = new VBox(15);

        vBox.getChildren().addAll( continueLvl, exit);
        vBox.setAlignment(Pos.CENTER);

        menu = new BorderPane(vBox);
        menu.setStyle("-fx-background-color: rgba(0, 0, 0, 0.5);");

        menu.setPrefSize(1000, 686);

        return menu;
    }

}
