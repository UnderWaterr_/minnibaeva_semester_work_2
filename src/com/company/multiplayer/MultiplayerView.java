package com.company.multiplayer;

import com.company.GameApplication;
import com.company.gameField.Block;
import com.company.gameField.LevelData;
import com.company.service.PassedLevelsService;
import com.company.service.PassedLevelsServiceImpl;
import com.company.sprite.SecondPlayerAnimation;
import com.company.sprite.SpriteMultiplayerAnimation;
import com.company.view.BaseView;
import javafx.animation.AnimationTimer;
import javafx.beans.value.ChangeListener;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class MultiplayerView extends BaseView {
    public String title = "MultiPlayer Game";

    public Pane pane = null;
    Image backgroundImg;

    public HashMap<KeyCode, Boolean> keys = new HashMap<>();

    public ArrayList<Block> platforms = new ArrayList<>();

    public Pane appRoot = new Pane();
    public Pane gameRoot = new Pane();
    public Pane uiRoot = new Pane();
    Font font = Font.font("Courier New", FontWeight.BOLD, 30);

    public BorderPane menu;
    public double secondPlayerX;
    public double secondPlayerY;
    public int animationSide = 7;

    public MultiplayerPlayerView player;
    public MultiplayerPlayerView secondPlayer;
    public int levelWidth;
    public Text time;
    public ImageView imageViewHeartFirst;
    public ImageView imageViewHeartSecond;
    public ImageView imageViewHeartThird;
    public ImageView coinImage;
    public Text coinsText;
    private HBox hBox;
    private VBox vBoxPlayersInfo;
    Image heart;
    Image greyHeart;
    long startTime;
    public final GameApplication application = getApplication();
    public final PassedLevelsService service = new PassedLevelsServiceImpl();
    public AnimationTimer timer;
    public SpriteMultiplayerAnimation animation;
    private SecondPlayerAnimation secondPlayerAnimation;
    ChangeListener<Number> listener;
    private boolean deleteCoin = false;
    private int deleteCoinX;
    private int deleteCoinY;

    public int lvlNumber;
    private long endTime;
    private String nickname;
    private long endTimeSecondPlayer;
    private String nicknameSecondPlayer;
    private boolean secondPlayerIsDead;


    public MultiplayerView() throws Exception {
    }


    public String getTitle() {
        return title;
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView(1);
        }
        return pane;
    }

    private void initContent() {
        String[] levelData = null;

        switch (Integer.toString(this.lvlNumber).charAt(0)) {
            case '1':
                levelData = LevelData.LEVEL1;
                break;
            case '2':
                levelData = LevelData.LEVEL2;
                break;
            case '3':
                levelData = LevelData.LEVEL3;
                break;
        }
        try {
            backgroundImg = new Image(new FileInputStream("src/resources/2.jpg"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ImageView background = new ImageView(backgroundImg);
        background.setFitHeight(720);
        background.setFitWidth(1280);


        levelWidth = levelData[0].length() * Block.BLOCK_SIZE;

        for (int i = 0; i < levelData.length; i++) {
            String line = levelData[i];
            for (int j = 0; j < line.length(); j++) {
                switch (line.charAt(j)) {
                    case '0':
                        break;
                    case '1':
                        Block platform = null;

                        try {
                            platform = new Block(Block.BlockType.BRICK, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(platform);
                        gameRoot.getChildren().add(platform);
                        break;
                    case '2':
                        Block acid = null;

                        try {
                            acid = new Block(Block.BlockType.ACID, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(acid);
                        gameRoot.getChildren().add(acid);
                        break;
                    case '3':
                        Block door = null;

                        try {
                            door = new Block(Block.BlockType.DOOR, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(door);
                        gameRoot.getChildren().add(door);
                        break;
                    case '4':
                        Block liana = null;

                        try {
                            liana = new Block(Block.BlockType.LIANA, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(liana);
                        gameRoot.getChildren().add(liana);
                        break;
                    case '5':
                        Block ice = null;

                        try {
                            ice = new Block(Block.BlockType.ICE, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(ice);
                        gameRoot.getChildren().add(ice);
                        break;
                    case '6':
                        Block coin = null;

                        try {
                            coin = new Block(Block.BlockType.COIN, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(coin);
                        gameRoot.getChildren().add(coin);
                        break;

                }
            }
        }

        try {
            player = new MultiplayerPlayerView();
            player.multiplayerView = this;

            animation = new SpriteMultiplayerAnimation(player, service.getSkin(application.getMenuView().getNickname().getText()));
            animation.stayToTheRight();

            gameRoot.getChildren().add(player);

            player.tpToStart();

            secondPlayer = new MultiplayerPlayerView();
            secondPlayerAnimation = new SecondPlayerAnimation(secondPlayer, service.getSkin(application.secondPlayerName));

            secondPlayerAnimation.stayToTheRight();
            gameRoot.getChildren().add(secondPlayer);


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        listener = (obs, old, newValue) -> {

            int offset = newValue.intValue();

            if (offset > 477 && offset < levelWidth - 477) {
                gameRoot.setLayoutX(-(offset - 477));
            }

        };

        player.translateXProperty().addListener(listener);


        createUiRoot();
        appRoot.getChildren().addAll(background, gameRoot, uiRoot);
        try {
            menu = new GameMenuMultiplayerPane(this).createMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void createUiRoot() {
        VBox vBox = new VBox(5);

        try {
            heart = new Image(new FileInputStream("src/resources/11.png"));
            greyHeart = new Image(new FileInputStream("src/resources/12.png"));
            coinImage = new ImageView(new Image(new FileInputStream("src/resources/14.png")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imageViewHeartFirst = new ImageView(heart);
        imageViewHeartFirst.setFitHeight(40);
        imageViewHeartFirst.setFitWidth(40);


        imageViewHeartSecond = new ImageView(heart);
        imageViewHeartSecond.setFitHeight(40);
        imageViewHeartSecond.setFitWidth(40);

        imageViewHeartThird = new ImageView(heart);
        imageViewHeartThird.setFitHeight(40);
        imageViewHeartThird.setFitWidth(40);

        time = new Text("0");

        coinsText = new Text(Integer.toString(player.coins));
        coinsText.setFont(font);

        GridPane heartsPane = new GridPane();
        heartsPane.add(imageViewHeartFirst, 0, 0);
        heartsPane.add(imageViewHeartSecond, 1, 0);
        heartsPane.add(imageViewHeartThird, 2, 0);
        heartsPane.add(coinImage, 0, 2);
        heartsPane.add(coinsText, 1, 2);
        heartsPane.setPrefWidth(700);
        time.setFont(font);
        vBox.getChildren().addAll(time, heartsPane);

        hBox = new HBox();
        vBoxPlayersInfo = new VBox();
        hBox.getChildren().addAll(vBox, vBoxPlayersInfo);

        uiRoot.getChildren().addAll(hBox);

    }

    public void playerLostHeart() {
        player.lives -= 1;
        player.tpToStart();
        if (player.lives == 0) {
            player.tpToStart();
            endLvl(true);
        }

        deleteHeartsFromPane(player.lives, imageViewHeartFirst, greyHeart, imageViewHeartSecond, imageViewHeartThird);
    }

    public static void deleteHeartsFromPane(int lives, ImageView imageViewHeartFirst, Image greyHeart, ImageView imageViewHeartSecond, ImageView imageViewHeartThird) {
        switch (lives) {
            case (1):
                imageViewHeartFirst.setImage(greyHeart);
                break;
            case (2):
                imageViewHeartSecond.setImage(greyHeart);
                break;
            case (3):
                imageViewHeartThird.setImage(greyHeart);
                break;
        }
    }

    private void update() {

        if (isPressed(KeyCode.W) && player.getTranslateY() >= 1 && !player.isJumped && player.freezeTime == 0) {
            player.isJumped = true;
            player.jumpPlayer();
        }

        if (isPressed(KeyCode.A) && player.getTranslateX() >= 1) {
            player.movePlayerX(-3);
        }

        if (isPressed(KeyCode.D) && player.getTranslateX() + player.getPlayerSizeX() <= levelWidth - 1) {
            player.movePlayerX(3);
        }

        if (player.playerVelocity.getY() < 10) {
            player.playerVelocity = player.playerVelocity.add(0, 1);
        }

        player.movePlayerY((int) player.playerVelocity.getY());

        if (player.getTranslateY() > 780) {
            playerLostHeart();
        }
    }


    private boolean isPressed(KeyCode key) {
        return keys.getOrDefault(key, false);
    }

    public void endLvl(boolean isDead) {
        Text text;
        nickname = application.getMenuView().getNickname().getText();

        if (isDead) {
            endTime = -1;
            text = new Text("dead " + nickname);

        } else {
            endTime = System.currentTimeMillis() - startTime;
            text = new Text(((endTime) / 1000) / 60 + ":" +
                    ((endTime) / 1000) % 60 + " " + nickname);
        }

        player.translateXProperty().removeListener(listener);
        secondPlayer.translateXProperty().addListener(listener);

        service.saveCoins(nickname, player.coins);

        text.setFont(font);
        vBoxPlayersInfo.getChildren().add(text);

        application.getChatClient()
                .sendMessage("3 " + endTime + " " + nickname + " " + "\n");

        gameRoot.getChildren().remove(player);

    }

    public void endLvlWithoutSecondPlayer() {
        endTime = System.currentTimeMillis() - startTime;
        nickname = application.getMenuView().getNickname().getText();
        service.saveCoins(nickname, player.coins);
    }

    public void endLvlSecondPlayer(long endTimeSecondPlayer, String nicknameSecondPlayer) {
        this.secondPlayerIsDead = true;
        this.endTimeSecondPlayer = endTimeSecondPlayer;
        this.nicknameSecondPlayer = nicknameSecondPlayer;
    }

    public void showMenu() {
        appRoot.getChildren().add(menu);
    }

    public void hideMenu() {
        appRoot.getChildren().remove(menu);
        timer.start();
        if (player.freezeTime != 0) {
            animation.freeze();
            application.getChatClient()
                    .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 6" + "\n");

        } else {
            animation.stayToTheRight();
            application.getChatClient()
                    .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 4" + "\n");

        }
    }

    public void createView(int lvlNumber) {
        this.lvlNumber = lvlNumber;
        initContent();

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                secondPlayer.setTranslateX(secondPlayerX);
                secondPlayer.setTranslateY(secondPlayerY);

                switch (animationSide) {
                    case 7:
                        break;
                    case 0:
                        secondPlayerAnimation.runToTheRight();
                        animationSide = 7;
                        break;
                    case 1:
                        secondPlayerAnimation.runToTheLeft();
                        animationSide = 7;
                        break;
                    case 2:
                        secondPlayerAnimation.jumpToTheRight();
                        animationSide = 7;
                        break;
                    case 3:
                        secondPlayerAnimation.jumpToTheLeft();
                        animationSide = 7;
                        break;
                    case 4:
                        secondPlayerAnimation.stayToTheRight();
                        animationSide = 7;
                        break;
                    case 5:
                        secondPlayerAnimation.stayToTheLeft();
                        animationSide = 7;
                        break;
                    case 6:
                        secondPlayerAnimation.freeze();
                        animationSide = 7;
                        break;
                }

                if (deleteCoin) {
                    for (Block platform : platforms) {
                        if (platform.positionX == deleteCoinX && platform.positionY == deleteCoinY) {
                            platforms.remove(platform);
                            gameRoot.getChildren().remove(platform);
                            deleteCoin = false;
                            break;
                        }
                    }
                }

                if (secondPlayerIsDead) {
                    Text text;
                    if (endTimeSecondPlayer == -1) {
                        text = new Text("dead " + nicknameSecondPlayer);

                    } else {
                        text = new Text(((endTimeSecondPlayer) / 1000) / 60 + ":" +
                                ((endTimeSecondPlayer) / 1000) % 60 + " " + nicknameSecondPlayer);

                    }
                    text.setFont(font);

                    vBoxPlayersInfo.getChildren().addAll(text);
                    secondPlayerIsDead = false;
                    gameRoot.getChildren().remove(secondPlayer);

                }
                update();

                if (player.freezeTime != 0) {
                    if (System.currentTimeMillis() - player.freezeTime > 1000) {
                        setPlayerSide();
                        player.freezeTime = 0;
                    }
                }
                time.setText(((System.currentTimeMillis() - startTime) / 1000) / 60 + ":" +
                        ((System.currentTimeMillis() - startTime) / 1000) % 60);
            }
        };

        Scene scene = new Scene(appRoot);
        scene.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                if (!appRoot.getChildren().contains(menu)) {
                    showMenu();
                } else {
                    hideMenu();
                }
            }

            if (player.freezeTime == 0 && !appRoot.getChildren().contains(menu)) {

                if (event.getCode().equals(KeyCode.W) && player.canJump && !player.isJumped) {
                    if (player.isRightSide) {
                        animation.jumpToTheRight();
                        application.getChatClient()
                                .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 2" + "\n");

                    } else {
                        animation.jumpToTheLeft();
                        application.getChatClient()
                                .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 3" + "\n");


                    }
                } else {
                    if (event.getCode().equals(KeyCode.D)
                            && !keys.getOrDefault(KeyCode.D, false)
                    ) {
                        animation.runToTheRight();
                        application.getChatClient()
                                .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 0" + "\n");

                    } else if (event.getCode().equals(KeyCode.A)
                            && !keys.getOrDefault(KeyCode.A, false)
                    ) {
                        animation.runToTheLeft();
                        application.getChatClient()
                                .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 1" + "\n");

                    }
                }

                if (event.getCode().equals(KeyCode.D)) {
                    player.isRightSide = true;
                } else if (event.getCode().equals(KeyCode.A)) {
                    player.isRightSide = false;
                }
                keys.put(event.getCode(), true);

            }


        });

        scene.setOnKeyReleased(event -> {
            if (!appRoot.getChildren().contains(menu)) {

                keys.put(event.getCode(), false);
                if (event.getCode().equals(KeyCode.W)) {
                    player.isJumped = false;
                }
                if (player.freezeTime == 0 && !appRoot.getChildren().contains(menu)) {

                    if (!keys.getOrDefault(KeyCode.D, false) &&
                            !keys.getOrDefault(KeyCode.W, false) &&
                            !keys.getOrDefault(KeyCode.A, false) &&
                            player.canJump) {
                        setPlayerSide();
                    }
                }

            }
        });

        application.getStage().setScene(scene);
        application.getStage().show();
        application.getStage().setWidth(970);
        application.getStage().setHeight(686);

        startTime = System.currentTimeMillis();
        timer.start();
    }

    private void setPlayerSide() {
        if (player.isRightSide) {
            animation.stayToTheRight();
            application.getChatClient()
                    .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 4" + "\n");

        } else {
            animation.stayToTheLeft();
            application.getChatClient()
                    .sendMessage("0 " + player.getTranslateX() + " " + player.getTranslateY() + " 5" + "\n");

        }
    }

    public void deleteCoin(int x, int y) {
        deleteCoin = true;
        deleteCoinX = x;
        deleteCoinY = y;
    }


}
