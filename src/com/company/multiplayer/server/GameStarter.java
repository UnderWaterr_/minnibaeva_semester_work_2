package com.company.multiplayer.server;

import java.io.IOException;

public class GameStarter implements Runnable {
    ChatServer game;

    public GameStarter(ChatServer game) {
        this.game = game;
    }

    @Override
    public void run() {
        try {
            game.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}