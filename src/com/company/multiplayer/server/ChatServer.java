package com.company.multiplayer.server;

import java.io.*;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

public class ChatServer {

    private final int PORT;
    private ServerSocket socket;
    private final List<ClientThread> clients = new ArrayList<>();

    public ChatServer(int PORT) {
        this.PORT = PORT;
    }

    public ServerSocket getSocket() {
        return socket;
    }

    public List<ClientThread> getClients() {
        return clients;
    }

    public void start() throws IOException {
        try {
            socket = new ServerSocket(PORT);

        } catch (BindException e) {
            return;
        }

        while (true) {
            Socket clientSocket = socket.accept();

            BufferedReader input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), StandardCharsets.UTF_8));
            BufferedWriter output = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), StandardCharsets.UTF_8));

            ClientThread clientThread = new ClientThread(input, output, this);
            clients.add(clientThread);

            new Thread(clientThread).start();
        }
    }

    public void sendMessage(String message, ClientThread sender) throws IOException {
        for (ClientThread client : clients) {
            if (client.equals(sender)) {
                continue;
            }

            client.getOutput().write(message + "\n");
            client.getOutput().flush();
        }
    }

    public void removeClient(ClientThread client) {
        clients.remove(client);
    }
}
