package com.company.multiplayer;

import com.company.gameField.Block;
import javafx.geometry.Point2D;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class MultiplayerPlayerView extends Pane {

    private final Image playerImg = new Image(new FileInputStream("src/resources/7.png"));
    public ImageView playerView;
    public int PLAYER_SIZE_X = 52;
    public static int PLAYER_SIZE_Y = 40;

    public Point2D playerVelocity = new Point2D(0, 0);
    public boolean canJump = true;

    public boolean isJumped = false;
    public boolean isRightSide = true;
    public int lives = 4;
    public long freezeTime;
    public boolean canFreeze = true;
    public int coins = 0;
    public MultiplayerView multiplayerView;

    public MultiplayerPlayerView() throws FileNotFoundException {
        playerView = new ImageView(playerImg);
        playerView.setFitWidth(PLAYER_SIZE_X);
        playerView.setFitHeight(PLAYER_SIZE_Y);
        playerView.getProperties().put("alive", true);
        playerView.setViewport(new Rectangle2D(0, 0, 52, 40));

        getChildren().add(playerView);

    }

    public ImageView getPlayerView() {
        return playerView;
    }

    public void setPlayerView(Image image) {
        playerView.setImage(image);
    }

    public int getPlayerSizeX() {
        return PLAYER_SIZE_X;
    }


    public void movePlayerX(int value) {
        multiplayerView.application.getChatClient()
                .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 7" + "\n");

        boolean movingRight = value > 0;


        for (int i = 0; i < Math.abs(value); i++) {

            if (freezeTime != 0) {
                return;
            }
            for (Block platform : multiplayerView.platforms) {

                if (this.getBoundsInParent().intersects(platform.getBoundsInParent())) {
                    if (getCoin(platform)) return;

                    if (platform.blockType.equals(Block.BlockType.ICE) && canFreeze) {
                        canFreeze = false;
                        freezeTime = System.currentTimeMillis();
                        multiplayerView.animation.freeze();
                        multiplayerView.application.getChatClient()
                                .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 6" + "\n");

                        return;
                    }

                    if (!canFreeze && !platform.blockType.equals(Block.BlockType.ICE)) {
                        canFreeze = true;
                    }


                    if (platform.blockType.equals(Block.BlockType.ACID)) {
                        multiplayerView.playerLostHeart();
                        return;
                    }

                    if (platform.blockType.equals(Block.BlockType.DOOR)) {
                        multiplayerView.endLvl(false);
                        tpToStart();
                        return;
                    }


                    if (movingRight && canFreeze) {
                        if (this.getTranslateX() + this.getPlayerSizeX() == platform.getTranslateX()) {
                            if (!platform.blockType.equals(Block.BlockType.LIANA)) {
                                this.setTranslateX(this.getTranslateX() - 1);
                            }
                            return;
                        }
                    } else {
                        if (this.getTranslateX() == platform.getTranslateX() + Block.BLOCK_SIZE) {
                            if (!platform.blockType.equals(Block.BlockType.LIANA)) {
                                this.setTranslateX(this.getTranslateX() + 1);
                            }
                            return;
                        }
                    }
                }
            }
            this.setTranslateX(this.getTranslateX() + (movingRight ? 1 : -1));
        }
    }

    private boolean getCoin(Block platform) {
        if (platform.blockType.equals(Block.BlockType.COIN)) {
            multiplayerView.application.getChatClient()
                    .sendMessage("2 " + platform.positionX + " " + platform.positionY + "\n");

            coins += 1;
            multiplayerView.coinsText.setText(Integer.toString(coins));
            multiplayerView.platforms.remove(platform);
            multiplayerView.gameRoot.getChildren().remove(platform);
            return true;
        }
        return false;
    }

    public void movePlayerY(int value) {
        multiplayerView.application.getChatClient()
                .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 7" + "\n");

        boolean movingDown = value > 0;

        for (int i = 0; i < Math.abs(value); i++) {
            for (Block platform : multiplayerView.platforms) {

                if (this.getBoundsInParent().intersects(platform.getBoundsInParent())) {

                    if (getCoin(platform)) return;

                    if (platform.blockType.equals(Block.BlockType.ICE) && canFreeze) {
                        canFreeze = false;
                        freezeTime = System.currentTimeMillis();
                        multiplayerView.animation.freeze();
                        multiplayerView.application.getChatClient()
                                .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 6" + "\n");

                    }

                    if (!canFreeze && !platform.blockType.equals(Block.BlockType.ICE)) {
                        canFreeze = true;
                    }

                    if (platform.blockType.equals(Block.BlockType.ACID)) {
                        multiplayerView.playerLostHeart();
                        return;
                    }

                    if (movingDown) {
                        if (this.getTranslateY() + MultiplayerPlayerView.PLAYER_SIZE_Y == platform.getTranslateY()) {
                            if (!platform.blockType.equals(Block.BlockType.LIANA)) {
                                this.setTranslateY(this.getTranslateY() - 1);
                            }
                            if (!canJump && freezeTime == 0) {
                                if (isRightSide) {
                                    multiplayerView.animation.stayToTheRight();
                                    multiplayerView.application.getChatClient()
                                            .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 4" + "\n");

                                } else {
                                    multiplayerView.animation.stayToTheLeft();
                                    multiplayerView.application.getChatClient()
                                            .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 5" + "\n");

                                }
                                if (multiplayerView.keys.getOrDefault(KeyCode.D, false)) {
                                    multiplayerView.animation.runToTheRight();
                                    multiplayerView.application.getChatClient()
                                            .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 0" + "\n");

                                } else if (multiplayerView.keys.getOrDefault(KeyCode.A, false)) {
                                    multiplayerView.animation.runToTheLeft();
                                    multiplayerView.application.getChatClient()
                                            .sendMessage("0 " + this.getTranslateX() + " " + this.getTranslateY() + " 1" + "\n");

                                }
                            }
                            canJump = true;
                            return;
                        }
                    }
                    if (!movingDown) {
                        if (this.getTranslateY() == platform.getTranslateY() + Block.BLOCK_SIZE) {
                            if (!platform.blockType.equals(Block.BlockType.LIANA)) {
                                this.setTranslateY(this.getTranslateY() + 1);
                            }
                            return;
                        }
                    }
                }
            }
            this.setTranslateY(this.getTranslateY() + (movingDown ? 1 : -1));

        }
    }

    public void jumpPlayer() {
        if (canJump) {
            playerVelocity = playerVelocity.add(0, -30);
            canJump = false;
        }
    }

    public void tpToStart() {

        this.setTranslateX(0);
        this.setTranslateY(200);
        multiplayerView.gameRoot.setLayoutX(0);
    }


}
