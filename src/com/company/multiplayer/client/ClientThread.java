package com.company.multiplayer.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;

public class ClientThread implements Runnable {

    private final BufferedReader input;

    private final BufferedWriter output;

    private final ChatClient client;

    public ClientThread(BufferedReader input, BufferedWriter output, ChatClient client) {
        this.input = input;
        this.output = output;
        this.client = client;
    }

    public BufferedWriter getOutput() {
        return output;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String message = input.readLine();

                if (Integer.parseInt(message.split(" ")[0]) == 0) {
                    client.getApplication().getMultiplayerView().secondPlayerX = Double.parseDouble(message.split(" ")[1]);
                    client.getApplication().getMultiplayerView().secondPlayerY = Double.parseDouble(message.split(" ")[2]);
                    if (Integer.parseInt(message.split(" ")[3]) != 7) {
                        client.getApplication().getMultiplayerView().animationSide = Integer.parseInt(message.split(" ")[3]);
                    }
                }

                if (Integer.parseInt(message.split(" ")[0]) == 2) {
                    client.getApplication().getMultiplayerView().deleteCoin(Integer.parseInt(message.split(" ")[1]),
                            Integer.parseInt(message.split(" ")[2]));
                }

                if (Integer.parseInt(message.split(" ")[0]) == 1) {
                    client.getApplication().lvlNumber = Integer.parseInt(message.split(" ")[1]);
                    client.getApplication().secondPlayerName = message.split(" ")[2];
                }

                if (Integer.parseInt(message.split(" ")[0]) == 3){
                    client.getApplication().getMultiplayerView().endLvlSecondPlayer(
                            Integer.parseInt(message.split(" ")[1]),
                            message.split(" ")[2]
                    );
                }

            }
        } catch (Exception ignored) {
        }
    }
}
