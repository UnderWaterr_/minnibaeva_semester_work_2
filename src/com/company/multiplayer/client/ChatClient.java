package com.company.multiplayer.client;

import com.company.GameApplication;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;
import java.nio.charset.StandardCharsets;

public class ChatClient {

    private Socket socket;
    private ClientThread clientThread;

    private final GameApplication application;

    public GameApplication getApplication() {
        return application;
    }

    public ChatClient(GameApplication application) {
        this.application = application;
    }


    public void sendMessage(String message) {
        try {
            clientThread.getOutput().write(message);
            clientThread.getOutput().flush();
        }  catch (SocketException e) {
            try {
                application.getMultiplayerView().endLvlWithoutSecondPlayer();
                application.getMultiplayerView().timer.stop();
                application.endMultiplayerGame();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void start(int port) throws IOException {

        socket = new Socket("127.0.0.1", port);

        BufferedWriter output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), StandardCharsets.UTF_8));
        BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream(), StandardCharsets.UTF_8));

        clientThread = new ClientThread(input, output, this);

       new Thread(clientThread).start();
    }

}
