package com.company.DTO;

public class PlayerPassedLevelsDTO {
    private String nickname;
    private LevelDTO level;

    public PlayerPassedLevelsDTO(String nickname, LevelDTO level) {
        this.nickname = nickname;
        this.level = level;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public LevelDTO getLevel() {
        return level;
    }

}
