package com.company.DTO;

public class LevelDTO {
    private final int levelNumber;
    private final long resultTime;

    public LevelDTO(int levelNumber, long resultTime) {
        this.levelNumber = levelNumber;
        this.resultTime = resultTime;
    }

    public int getLevelNumber() {
        return levelNumber;
    }

    public long getResultTime() {
        return resultTime;
    }

}
