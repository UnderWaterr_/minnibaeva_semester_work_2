package com.company.singleplayer;

import com.company.GameApplication;
import com.company.gameField.Block;
import com.company.gameField.LevelData;
import com.company.multiplayer.MultiplayerView;
import com.company.service.PassedLevelsService;
import com.company.service.PassedLevelsServiceImpl;
import com.company.sprite.SpriteAnimation;
import com.company.view.BaseView;
import javafx.animation.AnimationTimer;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;

public class SinglePlayerGameView extends BaseView {

    public String title = "SinglePlayer Game";

    public Pane pane = null;
    Image backgroundImg;

    public HashMap<KeyCode, Boolean> keys = new HashMap<>();

    public ArrayList<Block> platforms = new ArrayList<>();

    public Pane appRoot = new Pane();
    public Pane gameRoot = new Pane();
    public Pane uiRoot = new Pane();
    Font font = Font.font("Courier New", FontWeight.BOLD, 30);

    public BorderPane menu;

    public PlayerView player;
    public int levelWidth;
    public Text time;
    public ImageView imageViewHeartFirst;
    public ImageView imageViewHeartSecond;
    public ImageView imageViewHeartThird;
    public ImageView coinImage;
    public Text coinsText;
    Image heart;
    Image greyHeart;
    long startTime;
    private long timeBeforePause = 0;
    public final GameApplication application = getApplication();
    private final PassedLevelsService service = new PassedLevelsServiceImpl();
    private AnimationTimer timer;
    public SpriteAnimation animation;
    public int lvlNumber;

    public SinglePlayerGameView() throws Exception {
    }

    public String getTitle() {
        return title;
    }

    @Override
    public Parent getView() {
        if (pane == null) {
            this.createView(1);
        }
        return pane;
    }

    private void initContent() {
        String[] levelData = null;

        switch (Integer.toString(this.lvlNumber).charAt(0)) {
            case '1':
                levelData = LevelData.LEVEL1;
                break;
            case '2':
                levelData = LevelData.LEVEL2;
                break;
            case '3':
                levelData = LevelData.LEVEL3;
                break;
        }

        try {
            backgroundImg = new Image(new FileInputStream("src/resources/2.jpg"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        ImageView background = new ImageView(backgroundImg);
        background.setFitHeight(720);
        background.setFitWidth(1280);


        levelWidth = levelData[0].length() * Block.BLOCK_SIZE;

        for (int i = 0; i < levelData.length; i++) {
            String line = levelData[i];
            for (int j = 0; j < line.length(); j++) {
                switch (line.charAt(j)) {
                    case '0':
                        break;
                    case '1':
                        Block platform = null;

                        try {
                            platform = new Block(Block.BlockType.BRICK, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(platform);
                        gameRoot.getChildren().add(platform);
                        break;
                    case '2':
                        Block acid = null;

                        try {
                            acid = new Block(Block.BlockType.ACID, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(acid);
                        gameRoot.getChildren().add(acid);
                        break;
                    case '3':
                        Block door = null;

                        try {
                            door = new Block(Block.BlockType.DOOR, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(door);
                        gameRoot.getChildren().add(door);
                        break;
                    case '4':
                        Block liana = null;

                        try {
                            liana = new Block(Block.BlockType.LIANA, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(liana);
                        gameRoot.getChildren().add(liana);
                        break;
                    case '5':
                        Block ice = null;

                        try {
                            ice = new Block(Block.BlockType.ICE, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(ice);
                        gameRoot.getChildren().add(ice);
                        break;
                    case '6':
                        Block coin = null;

                        try {
                            coin = new Block(Block.BlockType.COIN, j, i);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        platforms.add(coin);
                        gameRoot.getChildren().add(coin);
                        break;

                }
            }
        }

        try {
            player = new PlayerView();
            player.singlePlayerGameView = this;

            animation = new SpriteAnimation(player, service.getSkin(application.getMenuView().getNickname().getText()));

            animation.stayToTheRight();

            gameRoot.getChildren().add(player);

            player.tpToStart();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        player.translateXProperty().addListener((obs, old, newValue) -> {
            int offset = newValue.intValue();

            if (offset > 477 && offset < levelWidth - 477) {
                gameRoot.setLayoutX(-(offset - 477));
            }
        });
        createUiRoot();
        appRoot.getChildren().addAll(background, gameRoot, uiRoot);
        try {
            menu = new SinglePlayerMenuView(this).createMenu();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void createUiRoot() {
        VBox vBox = new VBox(5);

        try {
            heart = new Image(new FileInputStream("src/resources/11.png"));
            greyHeart = new Image(new FileInputStream("src/resources/12.png"));
            coinImage = new ImageView(new Image(new FileInputStream("src/resources/14.png")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        imageViewHeartFirst = new ImageView(heart);
        imageViewHeartFirst.setFitHeight(40);
        imageViewHeartFirst.setFitWidth(40);


        imageViewHeartSecond = new ImageView(heart);
        imageViewHeartSecond.setFitHeight(40);
        imageViewHeartSecond.setFitWidth(40);

        imageViewHeartThird = new ImageView(heart);
        imageViewHeartThird.setFitHeight(40);
        imageViewHeartThird.setFitWidth(40);

        time = new Text("0");

        coinsText = new Text(Integer.toString(player.coins));
        coinsText.setFont(font);

        GridPane heartsPane = new GridPane();
        heartsPane.add(imageViewHeartFirst, 0, 0);
        heartsPane.add(imageViewHeartSecond, 1, 0);
        heartsPane.add(imageViewHeartThird, 2, 0);
        heartsPane.add(coinImage, 0, 2);
        heartsPane.add(coinsText, 1, 2);


        time.setFont(font);
        vBox.getChildren().addAll(time, heartsPane);
        uiRoot.getChildren().add(vBox);
    }

    public void playerLostHeart() {
        isRestart();
        player.lives -= 1;
        player.tpToStart();

        MultiplayerView.deleteHeartsFromPane(player.lives, imageViewHeartFirst, greyHeart, imageViewHeartSecond, imageViewHeartThird);
    }

    private void update() {


        if (isPressed(KeyCode.W) && player.getTranslateY() >= 1 && !player.isJumped && player.freezeTime == 0) {
            player.isJumped = true;
            player.jumpPlayer();
        }

        if (isPressed(KeyCode.A) && player.getTranslateX() >= 1) {
            player.movePlayerX(-3);
        }

        if (isPressed(KeyCode.D) && player.getTranslateX() + player.getPlayerSizeX() <= levelWidth - 1) {
            player.movePlayerX(3);
        }

        if (player.playerVelocity.getY() < 10) {
            player.playerVelocity = player.playerVelocity.add(0, 1);
        }

        player.movePlayerY((int) player.playerVelocity.getY());

        if (player.getTranslateY() > 780) {
            playerLostHeart();
        }

    }

    public void isRestart() {
        if (player.lives - 1 == 0) {
            application.startGame(this.lvlNumber);
        }
    }

    private boolean isPressed(KeyCode key) {
        return keys.getOrDefault(key, false);
    }

    public void endLvl() {
        service.save(application.getMenuView().getNickname().getText(), lvlNumber, System.currentTimeMillis() - startTime);
        service.saveCoins(application.getMenuView().getNickname().getText(), player.coins);
        application.endGame();
    }

    public void showMenu() {
        timeBeforePause += System.currentTimeMillis() - startTime;
        appRoot.getChildren().add(menu);
        timer.stop();
        animation.stop();
    }

    public void hideMenu() {
        startTime = System.currentTimeMillis();
        appRoot.getChildren().remove(menu);
        timer.start();
        if (player.freezeTime != 0) {
            animation.freeze();
        } else {
            animation.stayToTheRight();
        }
    }

    public void createView(int lvlNumber) {
        this.lvlNumber = lvlNumber;
        initContent();

        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                update();
                if (player.freezeTime != 0) {
                    if (System.currentTimeMillis() - player.freezeTime > 1000) {
                        if (player.isRightSide) {
                            animation.stayToTheRight();
                        } else {
                            animation.stayToTheLeft();

                        }
                        player.freezeTime = 0;
                    }
                }
                time.setText(((System.currentTimeMillis() - startTime + timeBeforePause) / 1000) / 60 + ":" +
                        ((System.currentTimeMillis() - startTime + timeBeforePause) / 1000) % 60);
            }
        };

        Scene scene = new Scene(appRoot);
        scene.setOnKeyPressed(event -> {
            if (event.getCode().equals(KeyCode.ESCAPE)) {
                if (!appRoot.getChildren().contains(menu)) {
                    showMenu();
                } else {
                    hideMenu();
                }
            }

            if (player.freezeTime == 0 && !appRoot.getChildren().contains(menu)) {

                if (event.getCode().equals(KeyCode.W) && player.canJump && !player.isJumped) {
                    if (player.isRightSide) {
                        animation.jumpToTheRight();
                    } else {
                        animation.jumpToTheLeft();

                    }
                } else {
                    if (event.getCode().equals(KeyCode.D)
                            && !keys.getOrDefault(KeyCode.D, false)
                            && !keys.getOrDefault(KeyCode.W, false)) {
                        animation.runToTheRight();
                    } else if (event.getCode().equals(KeyCode.A)
                            && !keys.getOrDefault(KeyCode.A, false)
                            && !keys.getOrDefault(KeyCode.D, false)) {
                        animation.runToTheLeft();
                    }
                }

                if (event.getCode().equals(KeyCode.D)) {
                    player.isRightSide = true;
                } else if (event.getCode().equals(KeyCode.A)) {
                    player.isRightSide = false;
                }
            }

            keys.put(event.getCode(), true);


        });

        scene.setOnKeyReleased(event -> {
            keys.put(event.getCode(), false);
            if (event.getCode().equals(KeyCode.W)) {
                player.isJumped = false;
            }
            if (player.freezeTime == 0 && !appRoot.getChildren().contains(menu)) {

                if (!keys.getOrDefault(KeyCode.D, false) &&
                        !keys.getOrDefault(KeyCode.W, false) &&
                        !keys.getOrDefault(KeyCode.A, false) &&
                        player.canJump) {
                    if (player.isRightSide) {
                        animation.stayToTheRight();
                    } else {
                        animation.stayToTheLeft();
                    }
                }
            }

        });

        application.getStage().setScene(scene);
        application.getStage().show();
        application.getStage().setWidth(970);
        application.getStage().setHeight(686);


        startTime = System.currentTimeMillis();
        timer.start();
    }


}
