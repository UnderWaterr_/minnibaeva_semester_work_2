package com.company.gameField;

import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class Block extends Pane {
    private final Image blocksImg = new Image(new FileInputStream("src/resources/1.png"));
    private final ImageView block;
    public static int BLOCK_SIZE = 54;
    public BlockType blockType;
    public int positionX;
    public int positionY;

    public enum BlockType {
        BRICK, DOOR, ACID, LIANA, ICE, COIN
    }

    public Block(BlockType blockType, int positionX, int positionY) throws FileNotFoundException {
        this.positionX = positionX;
        this.positionY = positionY;
        block = new ImageView(blocksImg);
        block.setFitWidth(BLOCK_SIZE);
        block.setFitHeight(BLOCK_SIZE);
        setTranslateX(positionX * BLOCK_SIZE);
        setTranslateY(positionY * BLOCK_SIZE);
        block.getProperties().put("alive", true);

        switch (blockType) {
            case BRICK:
                this.blockType = BlockType.BRICK;
                block.setViewport(new Rectangle2D(192, 0, 64, 64));
                break;
            case ACID:
                this.blockType = BlockType.ACID;
                block.setViewport(new Rectangle2D(832, 0, 64, 64));
                break;
            case DOOR:
                this.blockType = BlockType.DOOR;
                block.setViewport(new Rectangle2D(1078, 384, 54, 54));
                break;
            case LIANA:
                this.blockType = BlockType.LIANA;
                block.setViewport(new Rectangle2D(1024, 384, 54, 54));
                break;
            case ICE:
                this.blockType = BlockType.ICE;
                block.setViewport(new Rectangle2D(1132, 384, 54, 54));
                break;
            case COIN:
                this.blockType = BlockType.COIN;
                block.setViewport(new Rectangle2D(977, 16, 64, 64));
                break;
        }
        getChildren().add(block);
    }


}