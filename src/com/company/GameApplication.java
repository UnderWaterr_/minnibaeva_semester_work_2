package com.company;

import com.company.model.PlayerConfig;
import com.company.multiplayer.MultiplayerView;
import com.company.multiplayer.client.ChatClient;
import com.company.multiplayer.server.ChatServer;
import com.company.singleplayer.SinglePlayerGameView;
import com.company.view.*;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.io.IOException;

public class GameApplication extends Application {

    public int lvlNumber;
    public String secondPlayerName;
    Stage stage;

    private BorderPane rootLayout;

    private SignInView signInView;

    private SignUpView signUpView;

    private MenuView menuView;

    private SinglePlayerGameView singlePlayerGameView;

    public MultiplayerView multiplayerView;

    private PlayerConfig playerConfig;

    private Scene scene;

    public ChatClient chatClient;
    public ChatServer chatServer;

    public SignInView getUserConfigView() {
        return signInView;
    }

    public PlayerConfig getUserConfig() {
        return playerConfig;
    }

    public void setUserConfig(PlayerConfig playerConfig) {
        this.playerConfig = playerConfig;
    }

    public SignInView getSignInView() {
        return signInView;
    }

    public MenuView getMenuView() {
        return menuView;
    }

    public Stage getStage() {
        return stage;
    }

    public BaseView getSignUpView() {
        return signUpView;
    }

    public MultiplayerView getMultiplayerView() {
        return multiplayerView;
    }

    public void setApplicationSize(int width, int height) {
        this.getStage().setWidth(width);
        this.getStage().setHeight(height);
    }

    public PlayerConfig getPlayerConfig() {
        return playerConfig;
    }

    public void setApplicationHeight(int height) {
        this.getStage().setHeight(height);
    }

    @Override
    public void start(Stage stage) throws Exception {
        this.stage = stage;
        this.stage.setTitle("Log in");
        this.stage.setOnCloseRequest(e -> System.exit(0));

        BaseView.setApplication(this);

        this.signInView = new SignInView();
        this.menuView = new MenuView();
        this.signUpView = new SignUpView();
        this.initLayout();

    }

    private void initLayout() {
        rootLayout = new BorderPane();

        scene = new Scene(rootLayout, 400, 600);
        stage.setScene(scene);
        stage.show();

        this.setView(getUserConfigView());
    }


    public void startMultiplayerGame(int levelNumber) {

        try {
            this.multiplayerView = new MultiplayerView();
        } catch (Exception e) {
            e.printStackTrace();
        }

        multiplayerView.createView(levelNumber);

        this.stage.setTitle("Game");
    }

    public void endMultiplayerGame() {
        try {
            chatServer.getSocket().close();
        } catch (IOException | NullPointerException ignored) {
        }
        chatClient = null;
        chatServer = null;
        secondPlayerName = null;
        lvlNumber = 0;
        stage.setScene(scene);
        stage.setWidth(220);
        stage.setHeight(240);

        stage.show();

        try {
            this.setView(new MenuView());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public ChatClient getChatClient() {
        return chatClient;
    }


    public void startChatClient(int port) throws IOException {
        getChatClient().start(port);
    }

    public void startGame(int levelNumber) {


        this.stage.setTitle("Game");
        try {
            this.singlePlayerGameView = new SinglePlayerGameView();
        } catch (Exception e) {
            e.printStackTrace();
        }
        singlePlayerGameView.createView(levelNumber);
    }

    public void endGame() {
        this.singlePlayerGameView = null;

        stage.setScene(scene);
        stage.setWidth(220);
        stage.setHeight(240);

        stage.show();
        this.setView(getMenuView());
    }

    public void setView(BaseView view) {
        this.stage.setTitle(view.getTitle());
        rootLayout.setCenter(view.getView());
    }


    public static void main(String[] args) {
        launch(args);

    }

    public void setChatServer(ChatServer chatServer) {
        this.chatServer = chatServer;
    }
}
