package com.company.sprite;

import com.company.singleplayer.PlayerView;
import javafx.animation.Animation;
import javafx.animation.Interpolator;
import javafx.animation.Transition;
import javafx.geometry.Rectangle2D;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class SpriteAnimation extends Transition {

    private final PlayerView player;
    private final ImageView imageView;
    private int count;
    private int columns;
    private final int offsetX;
    private final int offsetY;
    private int width;
    private int height;

    private int lastIndex;

    private static Image runToTheRightImage = null;

    private static Image runToTheLeftImage = null;

    private static Image jumpToTheRightImage = null;

    private static Image jumpToTheLeftImage = null;

    private static Image stayToTheRightImage = null;

    private static Image stayToTheLeftImage = null;

    private static Image freezeImage = null;

    public SpriteAnimation(PlayerView player, int skin) {
        this.player = player;
        this.imageView = player.getPlayerView();
        this.offsetX = 0;
        this.offsetY = 0;
        this.width = 52;
        this.height = 40;

        setCycleDuration(Duration.millis(1000));
        setInterpolator(Interpolator.LINEAR);

        try {
            if (skin == 1) {

                stayToTheRightImage = new Image(new FileInputStream("src/resources/8.png"));

                stayToTheLeftImage = new Image(new FileInputStream("src/resources/10.png"));

                jumpToTheLeftImage = new Image(new FileInputStream("src/resources/9.png"));

                jumpToTheRightImage = new Image(new FileInputStream("src/resources/7.png"));

                runToTheLeftImage = new Image(new FileInputStream("src/resources/5.png"));

                runToTheRightImage = new Image(new FileInputStream("src/resources/6.png"));

            }
            if (skin == 2) {
                stayToTheRightImage = new Image(new FileInputStream("src/resources/19.png"));

                stayToTheLeftImage = new Image(new FileInputStream("src/resources/21.png"));

                jumpToTheLeftImage = new Image(new FileInputStream("src/resources/20.png"));

                jumpToTheRightImage = new Image(new FileInputStream("src/resources/18.png"));

                runToTheLeftImage = new Image(new FileInputStream("src/resources/17.png"));

                runToTheRightImage = new Image(new FileInputStream("src/resources/16.png"));

            }
            freezeImage = new Image(new FileInputStream("src/resources/13.png"));

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    //"0"
    public void runToTheRight() {
        this.count = 8;
        this.columns = 8;
        player.setPlayerView(runToTheRightImage);
        this.setCycleCount(Animation.INDEFINITE);
        this.play();
    }

    //"1"
    public void runToTheLeft() {
        this.count = 8;
        this.columns = 8;
        player.setPlayerView(runToTheLeftImage);
        this.setCycleCount(Animation.INDEFINITE);
        this.play();
    }

    //"2"
    public void jumpToTheRight() {
        this.count = 11;
        this.columns = 11;
        player.setPlayerView(jumpToTheRightImage);
        this.setCycleCount(1);
        this.play();
    }

    //"3"
    public void jumpToTheLeft() {
        this.count = 11;
        this.columns = 11;
        player.setPlayerView(jumpToTheLeftImage);
        this.setCycleCount(1);
        this.play();
    }

    //"4"
    public void stayToTheRight() {
        this.count = 5;
        this.columns = 5;
        player.setPlayerView(stayToTheRightImage);
        this.setCycleCount(Animation.INDEFINITE);
        this.play();
    }

    //"5"
    public void stayToTheLeft() {
        this.count = 5;
        this.columns = 5;
        player.setPlayerView(stayToTheLeftImage);
        this.setCycleCount(Animation.INDEFINITE);
        this.play();
    }

    //"6"
    public void freeze() {
        this.count = 1;
        this.columns = 1;
        player.setPlayerView(freezeImage);
        this.setCycleCount(Animation.INDEFINITE);
        this.play();
    }

    protected void interpolate(double k) {
        final int index = Math.min((int) Math.floor(k * count), count - 1);
        if (index != lastIndex) {
            final int x = (index % columns) * width + offsetX;
            final int y = (index / columns) * height + offsetY;
            imageView.setViewport(new Rectangle2D(x, y, width, height));
            lastIndex = index;
        }
    }


}
