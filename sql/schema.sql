create table player
(
    nickname   varchar(30) not null UNIQUE,
    password   varchar(32) not null,
    coin       int         not null default 0,
    skin_number int not null default 1,
    is_skin_buyed bool default false
);

create table passed_level
(
    nickname     varchar(30) not null,
    level_number int         not null,
    time         int         not null
);